package com.vsdeni.sfparks;

import android.database.Cursor;
import android.location.Location;
import android.test.InstrumentationTestCase;

import com.vsdeni.sfparks.database.ContentDescriptor;
import com.vsdeni.sfparks.fragments.ParkListFragment;
import com.vsdeni.sfparks.model.Park;
import com.vsdeni.sfparks.network.SfgovClient;
import com.vsdeni.sfparks.network.SfgovServiceGenerator;

import org.junit.Assert;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

/**
 * Created by Deni on 26.02.2016.
 */
public class DatabaseTest extends InstrumentationTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        testLoadFromServerAndSaveToDb();
    }

    /**
     * Checks that after sorting alphabeticaly parks are ordered correctly
     */
    public void testSortingAlpabetically() {
        String sortOrder = ParkListFragment.getOrderByClause(ParkListFragment.Order.ALPHABETICAL, null);
        Cursor cursor = getInstrumentation().getContext().getContentResolver()
                .query(ContentDescriptor.Parks.URI, null, null, null, sortOrder);

        assertNotNull(cursor);
        assertTrue(cursor.moveToFirst());

        char prevFirstLetter = 0;
        do {
            String name = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.NAME));
            assertNotNull(name);
            char curFirstLetter = name.charAt(0);
            assertTrue(curFirstLetter >= prevFirstLetter);
            prevFirstLetter = curFirstLetter;
        } while (cursor.moveToNext());
    }

    /**
     * Checks that after sorting by acreage parks are ordered correctly
     */
    public void testSortingByAcreage() {
        String sortOrder = ParkListFragment.getOrderByClause(ParkListFragment.Order.ACREAGE, null);
        Cursor cursor = getInstrumentation().getContext().getContentResolver()
                .query(ContentDescriptor.Parks.URI, null, null, null, sortOrder);

        assertNotNull(cursor);
        assertTrue(cursor.moveToFirst());

        float prevAcreage = 0;
        do {
            float curAcreage = cursor.getFloat(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.ACREAGE));
            assertTrue(curAcreage + ", " + prevAcreage, curAcreage >= prevAcreage);
            prevAcreage = curAcreage;
        } while (cursor.moveToNext());
    }

    public void testSortingByDistance() {
        Location initLocation = new Location("");
        //near Alta Plaza Park
        initLocation.setLatitude(37.792164);
        initLocation.setLongitude(-122.436168);
        checkDistanceOrderFromLocation(initLocation, "alta");

        //near Union Square
        initLocation.setLatitude(37.788042);
        initLocation.setLongitude(-122.406272);
        checkDistanceOrderFromLocation(initLocation, "union");

        //near MOSCONE RECREATION CENTER
        initLocation.setLatitude(37.800285);
        initLocation.setLongitude(-122.432794);
        checkDistanceOrderFromLocation(initLocation, "moscone");
    }

    /**
     * @param initLocation - current user location
     * @param name         - part of expected nearest park name
     *                     Checks that after sorting by distance expected park placed on the first place
     *                     Checks that after sorting by distance parks are ordered correctly
     */
    public void checkDistanceOrderFromLocation(Location initLocation, String name) {
        /**
         *since sorting and testing alghorithms are different let's allow
         * margin of distance error in meters
         **/
        final int marginOfError = 50;

        String sortOrder = ParkListFragment.getOrderByClause(ParkListFragment.Order.DISTANCE, initLocation);
        Cursor cursor = getInstrumentation().getContext().getContentResolver()
                .query(ContentDescriptor.Parks.URI, null, null, null, sortOrder);

        assertNotNull(cursor);
        assertTrue(cursor.moveToFirst());

        String nearestParkName = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.NAME));
        assertNotNull(nearestParkName);
        assertTrue("Expected: " + name + ", instead: " + nearestParkName, nearestParkName.toLowerCase().contains(name));

        Location destLocation = new Location("");
        float prevDistance = 0;
        do {
            double curLatitude = cursor.getDouble(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.LATITUDE));
            double curLongitude = cursor.getDouble(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.LONGITUDE));
            destLocation.setLatitude(curLatitude);
            destLocation.setLongitude(curLongitude);
            float curDistance = initLocation.distanceTo(destLocation);
            assertTrue(curDistance + ", " + prevDistance, curDistance + marginOfError >= prevDistance);
            prevDistance = curDistance;
        } while (cursor.moveToNext());
    }

    public void testLoadFromServerAndSaveToDb() {
        SfgovClient client = SfgovServiceGenerator.createService(SfgovClient.class);
        Call<List<Park>> call = client.getParks();
        List<Park> parks = null;
        try {
            parks = call.execute().body();
        } catch (IOException e) {
            Assert.fail("Exception " + e);
        }
        assertNotNull(parks);
        assertTrue(parks.size() > 0);
        int inserted = ParkListFragment.saveParksToDatabase(getInstrumentation().getContext(), parks);
        assertTrue(inserted > 0);
    }
}
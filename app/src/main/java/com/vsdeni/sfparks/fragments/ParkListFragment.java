package com.vsdeni.sfparks.fragments;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.vsdeni.sfparks.R;
import com.vsdeni.sfparks.activities.ToolbarActivity;
import com.vsdeni.sfparks.adapter.ParksAdapter;
import com.vsdeni.sfparks.database.ContentDescriptor;
import com.vsdeni.sfparks.events.InternetAvailableEvent;
import com.vsdeni.sfparks.events.LocationUpdatedEvent;
import com.vsdeni.sfparks.listeners.LastLocationListener;
import com.vsdeni.sfparks.model.Park;
import com.vsdeni.sfparks.network.SfgovClient;
import com.vsdeni.sfparks.network.SfgovServiceGenerator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class ParkListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {
    public static final int LOADER_ID = 1;

    public boolean loaderFinished = false;

    public static final String TAG = "ListFragment";
    private static final String ORDER_KEY = "order";

    /**
     * Executes saving parks runnables. To prevent simultaneous thread executing used SingleThreadExecutor
     */
    ExecutorService mSaveParksExecutor;

    Call<List<Park>> mGetParksCall;

    ParksAdapter mAdapter;
    LastLocationListener mLastLocationListener;
    OnParkSelectListener mOnParkSelectListener;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({Order.ALPHABETICAL, Order.DISTANCE, Order.ACREAGE})
    public @interface OrderType {
    }

    public static class Order {
        public static final int ALPHABETICAL = 0;
        public static final int DISTANCE = 1;
        public static final int ACREAGE = 2;
    }

    private static final int DEF_ORDER_TYPE = Order.DISTANCE;

    private
    @OrderType
    int mOrderType = DEF_ORDER_TYPE;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mOnParkSelectListener != null) {
            Cursor cursor = (Cursor) mAdapter.getItem(position);
            if (!cursor.isClosed()) {
                Park park = Park.fromCursor(cursor);
                mOnParkSelectListener.onParkSelected(park);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof LastLocationListener) {
            mLastLocationListener = (LastLocationListener) context;
        }
        if (context instanceof OnParkSelectListener) {
            mOnParkSelectListener = (OnParkSelectListener) context;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setOnItemClickListener(this);
    }

    private void sort(@OrderType int orderType) {
        Timber.i("sort " + orderType);
        mOrderType = orderType;
        initLoader();
    }

    private void initSortSpinner() {
        if (mLastLocationListener.getLocation() == null) {
            //if location still doesn't available won't let user to select sort type in spinner
            return;
        }
        Toolbar toolbar = ((ToolbarActivity) getActivity()).getToolbar();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sort_types, R.layout.support_simple_spinner_dropdown_item);
        Spinner spinner = (Spinner) toolbar.findViewById(R.id.sort_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                @OrderType int orderType;
                switch (position) {
                    case 2:
                        orderType = Order.ACREAGE;
                        break;
                    case 1:
                        orderType = Order.DISTANCE;
                        break;
                    default:
                        orderType = Order.ALPHABETICAL;
                        break;
                }
                if (orderType != Order.DISTANCE || mLastLocationListener.getLocation() != null) {
                    sort(orderType);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        int position;
        switch (mOrderType) {
            case Order.ACREAGE:
                position = 2;
                break;
            case Order.DISTANCE:
                position = 1;
                break;
            default:
                position = 0;
                break;
        }
        spinner.setAdapter(adapter);
        spinner.setSelection(position);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (getActivity() instanceof ToolbarActivity) {
            ((ToolbarActivity) getActivity()).updateToolbarWidgetsVisibility();
            initSortSpinner();
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().getSupportLoaderManager().destroyLoader(LOADER_ID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ParksAdapter(getActivity(), null, 0);

        if (savedInstanceState != null) {
            @OrderType int orderType = savedInstanceState.getInt(ORDER_KEY, DEF_ORDER_TYPE);
            mOrderType = orderType;
        }

        Timber.i("created " + toString() + ", type:" + mOrderType);

        setHasOptionsMenu(true);

        loadParksFromServer();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu.findItem(R.id.refresh) == null) {
            inflater.inflate(R.menu.menu_list, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void initLoader() {
        Timber.i("initLoader");
        getActivity().getSupportLoaderManager().restartLoader(LOADER_ID, null, this);
    }

    @Subscribe(sticky = true)
    public void onLocationUpdatedEvent(LocationUpdatedEvent event) {
        initSortSpinner();
        if (mOrderType == Order.DISTANCE) {
            initLoader();
        }
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Subscribe
    public void onInternetAvailableEvent(InternetAvailableEvent event) {
        if (event.isAvailable()) {
            loadParksFromServer();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                loadParksFromServer();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private synchronized void saveParksToDatabaseAsync(final List<Park> parks) {
        if (mSaveParksExecutor == null || mSaveParksExecutor.isShutdown()) {
            mSaveParksExecutor = Executors.newSingleThreadExecutor();
        }
        mSaveParksExecutor.submit(new Runnable() {
            @Override
            public void run() {
                if (parks != null) {
                    if (isAdded()) {
                        saveParksToDatabase(getActivity().getApplication(), parks);
                    }
                }
            }
        });
    }

    public static int saveParksToDatabase(Context context, List<Park> parks) {
        List<ContentValues> values = new ArrayList<>();
        for (int i = 0; i < parks.size(); i++) {
            Park park = parks.get(i);
            if (park.isValid()) {
                values.add(park.toContentValues());
            }
        }

        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.delete(ContentDescriptor.Parks.URI, null, null);
        int count = contentResolver.bulkInsert(ContentDescriptor.Parks.URI, values.toArray(new ContentValues[values.size()]));
        Timber.d("-----table updated-----");
        return count;
    }

    private void loadParksFromServer() {
        if (mGetParksCall != null && !mGetParksCall.isExecuted()) {
            //parks fetching already in progress
            Timber.d("-----new loading cancelled since execution in progress-----");
            return;
        }

        Timber.d("-----loadParksFromServer started-----");
        SfgovClient client = SfgovServiceGenerator.createService(SfgovClient.class);
        mGetParksCall = client.getParks();
        mGetParksCall.enqueue(new Callback<List<Park>>() {
            @Override
            public void onResponse(Call<List<Park>> call, Response<List<Park>> response) {
                List<Park> parks = response.body();
                saveParksToDatabaseAsync(parks);
            }

            @Override
            public void onFailure(Call<List<Park>> call, Throwable t) {
                Timber.d("-----loading failed-----");
            }
        });
    }

    public static String getOrderByClause(int orderType, Location location) {
        String sortOrder;
        switch (orderType) {
            case Order.ACREAGE:
                sortOrder = ContentDescriptor.Parks.Cols.ACREAGE;
                break;
            case Order.DISTANCE:
                //http://stackoverflow.com/a/7472230/1735100
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                double fudge = Math.pow(Math.cos(Math.toRadians(latitude)), 2);
                sortOrder = "(" + latitude + " - " + ContentDescriptor.Parks.Cols.LATITUDE + ")" +
                        "*" +
                        "(" + latitude + " - " + ContentDescriptor.Parks.Cols.LATITUDE + ")" +
                        "+" +
                        "(" + longitude + " - " + ContentDescriptor.Parks.Cols.LONGITUDE + ")" +
                        "*" +
                        "(" + longitude + " - " + ContentDescriptor.Parks.Cols.LONGITUDE + ")" +
                        "* " + fudge;
                break;
            default:
                sortOrder = ContentDescriptor.Parks.Cols.NAME;
        }

        return sortOrder;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Location location = mLastLocationListener.getLocation();
        String sortOrder = getOrderByClause(mOrderType, location);
        CursorLoader loader = new CursorLoader(getActivity(), ContentDescriptor.Parks.URI, null, null, null, sortOrder);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Timber.i("loadFinished" + data.getCount());
        loaderFinished = true;
        setListAdapter(mAdapter);
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public interface OnParkSelectListener {
        void onParkSelected(Park park);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ORDER_KEY, mOrderType);
    }
}
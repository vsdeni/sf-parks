package com.vsdeni.sfparks;

import android.os.Build;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.test.ActivityInstrumentationTestCase2;

import com.vsdeni.sfparks.activities.MainActivity;
import com.vsdeni.sfparks.fragments.ParkListFragment;

import junit.framework.Assert;

import timber.log.Timber;

/**
 * Created by Deni on 25.02.2016.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }

    MainActivity mActivity;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        initActivity();
    }

    public void initActivity() {
        mActivity = getActivity();
        assertNotNull(mActivity);
        allowPermissionsIfNeeded();
    }

    public void testLocationExists() {
        try {
            //wait while location initialized
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertNotNull(mActivity.getLocation());
    }

    public void testLoader() {
        try {
            //wait while fragment added
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ParkListFragment fragment = (ParkListFragment) mActivity.getSupportFragmentManager()
                .findFragmentByTag(ParkListFragment.TAG);

        assertNotNull(fragment);

        Loader<?> loader =
                mActivity.getSupportLoaderManager().getLoader(ParkListFragment.LOADER_ID);

        LoaderUtils.waitForLoader(loader);
        assertTrue(fragment.loaderFinished);
    }

    private void allowPermissionsIfNeeded() {
        if (Build.VERSION.SDK_INT >= 23) {
            UiObject allowPermissions = UiDevice
                    .getInstance(getInstrumentation())
                    .findObject(new UiSelector().text("Allow"));//FIX: won't work for other than EN locale
            if (allowPermissions.exists()) {
                try {
                    allowPermissions.click();
                } catch (UiObjectNotFoundException e) {
                    Timber.e(e, "There is no permissions dialog to interact with ");
                }
            }
        }
    }

    public static class LoaderUtils {

        public static void waitForLoader(Loader<?> loader) {

            final AsyncTaskLoader<?> asyncTaskLoader
                    = (AsyncTaskLoader<?>) loader;

            Thread waitThreads = new Thread("LoaderWaitingThread") {
                @Override
                public void run() {
                    try {
                        asyncTaskLoader.waitForLoader();
                    } catch (Throwable e) {
                        Assert.fail("Exception while waiting for loader: "
                                + asyncTaskLoader.getId());
                    }
                }
            };

            waitThreads.start();

            // Now we wait for all these threads to finish
            try {
                waitThreads.join();
            } catch (InterruptedException ignored) {
            }
        }
    }
}

package com.vsdeni.sfparks.listeners;

import android.location.Location;

/**
 * Created by denisvasilenko on 20.02.2016.
 */
public interface LastLocationListener {
    Location getLocation();

    void onUpdateLocation();
}

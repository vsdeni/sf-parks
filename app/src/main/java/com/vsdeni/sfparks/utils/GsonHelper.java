package com.vsdeni.sfparks.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.vsdeni.sfparks.model.Location;

import java.lang.reflect.Type;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class GsonHelper {
    public static final Gson GSON;
    private static final Gson CLEAR_GSON;

    static {
        GSON = new GsonBuilder()
                .registerTypeAdapter(Location.Address.class, new AddressDeserializer())
                .create();

        CLEAR_GSON = new Gson();
    }


    private static class AddressDeserializer implements JsonDeserializer<Location.Address> {

        @Override
        public Location.Address deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Location.Address address = CLEAR_GSON.fromJson(json.getAsString(), Location.Address.class);
            return address;
        }
    }
}

package com.vsdeni.sfparks.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.vsdeni.sfparks.database.ContentDescriptor;

import java.io.Serializable;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class Park implements Serializable {

    @SerializedName("parktype")
    private String mType;

    @SerializedName("parkname")
    private String mName;

    @SerializedName("email")
    private String mEmail;

    @SerializedName("zipcode")
    private int mZipCode;

    @SerializedName("parkid")
    private int mId;

    @SerializedName("supdist")
    private int mSupDist;

    @SerializedName("number")
    private String mPhoneNumber;

    @SerializedName("parkservicearea")
    private String mServiceArea;

    @SerializedName("location_1")
    private Location mLocation;

    @SerializedName("acreage")
    private float mAcreage;

    @SerializedName("psamanager")
    private String mManager;

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public int getZipCode() {
        return mZipCode;
    }

    public void setZipCode(int zipCode) {
        mZipCode = zipCode;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getSupDist() {
        return mSupDist;
    }

    public void setSupDist(int supDist) {
        mSupDist = supDist;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getServiceArea() {
        return mServiceArea;
    }

    public void setServiceArea(String serviceArea) {
        mServiceArea = serviceArea;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    public float getAcreage() {
        return mAcreage;
    }

    public void setAcreage(float acreage) {
        mAcreage = acreage;
    }

    public String getManager() {
        return mManager;
    }

    public void setManager(String manager) {
        mManager = manager;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues(15);
        contentValues.put(ContentDescriptor.Parks.Cols.ID, getId());
        contentValues.put(ContentDescriptor.Parks.Cols.NAME, getName());
        contentValues.put(ContentDescriptor.Parks.Cols.TYPE, getType());
        contentValues.put(ContentDescriptor.Parks.Cols.EMAIL, getEmail());
        contentValues.put(ContentDescriptor.Parks.Cols.ZIPCODE, getZipCode());
        contentValues.put(ContentDescriptor.Parks.Cols.SUP_DIST, getSupDist());
        contentValues.put(ContentDescriptor.Parks.Cols.PHONE, getPhoneNumber());
        contentValues.put(ContentDescriptor.Parks.Cols.AREA, getServiceArea());
        contentValues.put(ContentDescriptor.Parks.Cols.ACREAGE, getAcreage());
        contentValues.put(ContentDescriptor.Parks.Cols.MANAGER, getManager());

        Location location = getLocation();
        if (location != null) {
            contentValues.put(ContentDescriptor.Parks.Cols.LATITUDE, location.getLatitude());
            contentValues.put(ContentDescriptor.Parks.Cols.LONGITUDE, location.getLongitude());
            Location.Address address = location.getHumanAddress();
            if (address != null) {
                contentValues.put(ContentDescriptor.Parks.Cols.CITY, address.getCity());
                contentValues.put(ContentDescriptor.Parks.Cols.STATE, address.getState());
                contentValues.put(ContentDescriptor.Parks.Cols.ADDRESS, address.getAddress());
            }
        }

        return contentValues;
    }

    public static Park fromCursor(Cursor cursor) {
        Park park = new Park();
        park.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.ID)));
        park.setName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.NAME)));
        park.setType(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.TYPE)));
        park.setEmail(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.EMAIL)));
        park.setZipCode(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.ZIPCODE)));
        park.setSupDist(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.SUP_DIST)));
        park.setAcreage(cursor.getFloat(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.ACREAGE)));
        park.setManager(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.MANAGER)));
        park.setPhoneNumber(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.PHONE)));
        park.setServiceArea(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.AREA)));

        Location location = new Location();
        location.setLatitude(cursor.getDouble(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.LATITUDE)));
        location.setLongitude(cursor.getDouble(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.LONGITUDE)));
        Location.Address address = new Location.Address();
        address.setCity(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.CITY)));
        address.setState(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.STATE)));
        address.setAddress(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Parks.Cols.ADDRESS)));
        location.setHumanAddress(address);
        park.setLocation(location);
        return park;
    }

    /**
     * @return returns flag that this park has valid info
     */
    public boolean isValid() {
        return getId() > 0
                && getLocation() != null
                && getLocation().getLatitude() != 0
                && getLocation().getLongitude() != 0;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getId()).append(" - ").append(getName()).append(" (").append(getType()).append(")");
        return builder.toString();
    }
}

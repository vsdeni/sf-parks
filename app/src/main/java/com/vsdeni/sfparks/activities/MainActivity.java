package com.vsdeni.sfparks.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.vsdeni.sfparks.R;
import com.vsdeni.sfparks.events.InternetAvailableEvent;
import com.vsdeni.sfparks.events.LocationUpdatedEvent;
import com.vsdeni.sfparks.fragments.ParkDetailsFragment;
import com.vsdeni.sfparks.fragments.ParkListFragment;
import com.vsdeni.sfparks.listeners.LastLocationListener;
import com.vsdeni.sfparks.model.Park;
import com.vsdeni.sfparks.utils.NetworkUtil;

import org.greenrobot.eventbus.EventBus;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LastLocationListener, ToolbarActivity, ParkListFragment.OnParkSelectListener {

    private static final int PERMISSIONS_REQUEST_ACCESS_LOCATION = 1;

    /**
     * GoogleApiClient using for retrieve current user location
     */
    GoogleApiClient mGoogleApiClient;

    Location mLocation;
    Toolbar mToolbar;

    BroadcastReceiver mNetworkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isInitialStickyBroadcast() && !NetworkUtil.isOfflineMode(MainActivity.this)) {
                onInternetEnabled();
            }
        }
    };

    IntentFilter mNetworkChangeIntentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        if (getResources().getBoolean(R.bool.dual_pane)) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(ParkDetailsFragment.TAG);
            if (fragment != null) {
                getSupportFragmentManager().popBackStack();
            }
        }

        // If we are being restored from a previous state, then we don't
        // need to do anything and should return or we could end up with overlapping Fragments
        if (savedInstanceState != null) {
            return;
        }

        ParkListFragment listFragment = new ParkListFragment();
        listFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, listFragment, ParkListFragment.TAG)
                .commit();
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    /**
     * Depends on current visible fragment should be changed visibility of widgets insde toolbar
     */
    @Override
    public void updateToolbarWidgetsVisibility() {
        boolean isDualPane = getResources().getBoolean(R.bool.dual_pane);

        Spinner spinner = (Spinner) mToolbar.findViewById(R.id.sort_spinner);
        if (isDualPane) {
            spinner.setVisibility(View.VISIBLE);
        } else {
            Fragment detailedFragment = getSupportFragmentManager().findFragmentByTag(ParkDetailsFragment.TAG);
            if (detailedFragment != null && detailedFragment.isVisible()) {
                spinner.setVisibility(View.GONE);
            } else {
                spinner.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /**
     * Handling permission for access to location and retrieving last coordinates
     */
    private void requestLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, R.string.permission_location_rationale, Toast.LENGTH_LONG).show();
            }
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_LOCATION);
            return;
        }

        //Try to get location via GoogleApiClient - the fastest way
        if (LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient).isLocationAvailable()) {
            mLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            onUpdateLocation();
        } else {
            //If fast way doesn't work lets request location via LocationManager
            Toast.makeText(this, R.string.location_wait, Toast.LENGTH_SHORT).show();
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean isNetworkEnabled = locationManager.
                    isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            String provider;
            if (isNetworkEnabled) {
                provider = LocationManager.NETWORK_PROVIDER;
            } else {
                provider = LocationManager.GPS_PROVIDER;
            }
            locationManager.requestSingleUpdate(provider, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Timber.d("Location changed");
                    mLocation = location;
                    onUpdateLocation();
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Timber.d("Status changed");
                }

                @Override
                public void onProviderEnabled(String provider) {
                    Timber.d("Provider enabled");
                }

                @Override
                public void onProviderDisabled(String provider) {
                    Timber.d("Provider disabled");
                }
            }, Looper.myLooper());
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocationUpdate();
                } else {
                    Toast.makeText(this, R.string.permission_location_hint, Toast.LENGTH_SHORT);
                    finish();
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Timber.d("GoogleApiClient connected");
        requestLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Timber.d("GoogleApiClient suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Timber.e("GoogleApiClient " + connectionResult.getErrorMessage());
        if (connectionResult.getErrorCode() == ConnectionResult.API_UNAVAILABLE) {
            requestLocationUpdate();
        }
    }

    @Override
    public Location getLocation() {
        return mLocation;
    }

    @Override
    public void onUpdateLocation() {
        EventBus.getDefault().postSticky(new LocationUpdatedEvent(mLocation));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mNetworkReceiver, mNetworkChangeIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }
    }

    private void onInternetEnabled() {
        EventBus.getDefault().post(new InternetAvailableEvent(true));
    }

    @Override
    public void onParkSelected(Park park) {
        if (getResources().getBoolean(R.bool.dual_pane)) {
            ParkDetailsFragment detailsFragment = (ParkDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.details_park);
            // If description is available, we are in two pane layout
            // so we call the method in DetailsFragment to update its content
            detailsFragment.setPark(park);
        } else {
            ParkDetailsFragment fragment = ParkDetailsFragment.newInstance(park);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment, ParkDetailsFragment.TAG)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        //To proper handling backstack with fragments when Back button pressed
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}

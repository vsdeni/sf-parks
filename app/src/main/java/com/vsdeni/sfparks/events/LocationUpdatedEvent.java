package com.vsdeni.sfparks.events;

import android.location.Location;

/**
 * Created by denisvasilenko on 20.02.2016.
 */
public class LocationUpdatedEvent {
    private Location mLocation;

    public LocationUpdatedEvent(Location location) {
        mLocation = location;
    }

    public Location getLocation() {
        return mLocation;
    }
}

package com.vsdeni.sfparks.events;

/**
 * Created by denisvasilenko on 22.02.16.
 */
public class InternetAvailableEvent {
    private boolean mAvailable;
    public InternetAvailableEvent(boolean available) {
        mAvailable = available;
    }

    public boolean isAvailable() {
        return mAvailable;
    }
}

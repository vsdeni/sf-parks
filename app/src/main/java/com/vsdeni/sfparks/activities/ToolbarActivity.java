package com.vsdeni.sfparks.activities;

import android.support.v7.widget.Toolbar;

/**
 * Created by denisvasilenko on 21.02.2016.
 */
public interface ToolbarActivity {
    Toolbar getToolbar();

    void updateToolbarWidgetsVisibility();
}

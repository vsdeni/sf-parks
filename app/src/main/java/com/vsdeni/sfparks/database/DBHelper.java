package com.vsdeni.sfparks.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "sfparks.db";
    private static final int DB_VERSION = 1;

    private static DBHelper INSTANCE;

    public static synchronized DBHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DBHelper(context);
        }
        return INSTANCE;
    }

    private DBHelper(Context c) {
        super(c, DB_NAME, null, DB_VERSION);
    }

    private void createTables(SQLiteDatabase db) {
        String createTableQuery = "CREATE TABLE " + ContentDescriptor.Parks.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Parks.Cols.ID + " INTEGER,"
                + ContentDescriptor.Parks.Cols.NAME + " TEXT,"
                + ContentDescriptor.Parks.Cols.TYPE + " TEXT,"
                + ContentDescriptor.Parks.Cols.ACREAGE + " REAL,"
                + ContentDescriptor.Parks.Cols.AREA + " REAL,"
                + ContentDescriptor.Parks.Cols.EMAIL + " TEXT,"
                + ContentDescriptor.Parks.Cols.ADDRESS + " TEXT,"
                + ContentDescriptor.Parks.Cols.CITY + " TEXT,"
                + ContentDescriptor.Parks.Cols.STATE + " TEXT,"
                + ContentDescriptor.Parks.Cols.LATITUDE + " REAL,"
                + ContentDescriptor.Parks.Cols.LONGITUDE + " REAL,"
                + ContentDescriptor.Parks.Cols.MANAGER + " TEXT,"
                + ContentDescriptor.Parks.Cols.SUP_DIST + " INTEGER,"
                + ContentDescriptor.Parks.Cols.PHONE + " TEXT,"
                + ContentDescriptor.Parks.Cols.ZIPCODE + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    private void dropTables(SQLiteDatabase db) {
        dropTable(db, ContentDescriptor.Parks.TABLE_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    public void dropTable(SQLiteDatabase db, String table) {
        db.execSQL("DROP TABLE IF EXISTS " + table);
    }

}

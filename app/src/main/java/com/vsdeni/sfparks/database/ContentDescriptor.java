package com.vsdeni.sfparks.database;

import android.content.UriMatcher;
import android.net.Uri;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class ContentDescriptor {
    public static final String AUTHORITY = "com.vsdeni.sfparks.db.provider";
    public static final UriMatcher URI_MATCHER = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, Parks.TABLE_NAME, Parks.CODE_ALL);
        matcher.addURI(AUTHORITY, Parks.TABLE_NAME + "/#", Parks.CODE_ID);
        return matcher;
    }

    public static class Parks {
        public static final String TABLE_NAME = "parks";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 100;
        public static final int CODE_ID = 200;

        public static class Cols {
            public static final String ID = "id";
            public static final String TYPE = "type";
            public static final String NAME = "name";
            public static final String EMAIL = "email";
            public static final String ZIPCODE = "zipcode";
            public static final String SUP_DIST = "supdist";
            public static final String PHONE = "phone";
            public static final String AREA = "area";
            public static final String ACREAGE = "acreage";
            public static final String MANAGER = "manager";
            public static final String LONGITUDE = "longitude";
            public static final String LATITUDE = "latitude";
            public static final String ADDRESS = "address";
            public static final String CITY = "city";
            public static final String STATE = "state";
        }
    }

}
package com.vsdeni.sfparks.network;

import com.vsdeni.sfparks.utils.GsonHelper;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class SfgovServiceGenerator {

    public static final String API_BASE_URL = " https://data.sfgov.org/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonHelper.GSON));

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}
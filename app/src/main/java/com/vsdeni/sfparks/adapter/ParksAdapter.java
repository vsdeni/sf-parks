package com.vsdeni.sfparks.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.vsdeni.sfparks.R;
import com.vsdeni.sfparks.model.Park;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class ParksAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;

    public ParksAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.row_park, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.title = (TextView) view.findViewById(R.id.title);
        viewHolder.manager = (TextView) view.findViewById(R.id.manager);
        viewHolder.email = (TextView) view.findViewById(R.id.email);
        viewHolder.phone = (TextView) view.findViewById(R.id.phone);
        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Park park = Park.fromCursor(cursor);
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        if (park != null && viewHolder != null) {
            viewHolder.title.setText(park.getName());
            viewHolder.manager.setText(park.getManager());
            viewHolder.phone.setText(park.getPhoneNumber());
            viewHolder.email.setText(park.getEmail());
        }
    }

    private static class ViewHolder {
        TextView title;
        TextView manager;
        TextView email;
        TextView phone;
    }
}

package com.vsdeni.sfparks;

import android.test.InstrumentationTestCase;

import com.vsdeni.sfparks.model.Park;
import com.vsdeni.sfparks.network.SfgovClient;
import com.vsdeni.sfparks.network.SfgovServiceGenerator;

import org.junit.Assert;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

public class ApiTest extends InstrumentationTestCase {

    /**
     * Checks that server returning response that correctly parses to parks list
     */
    public void testGetParks() {
        SfgovClient client = SfgovServiceGenerator.createService(SfgovClient.class);
        Call<List<Park>> call = client.getParks();
        List<Park> parks = null;
        try {
            parks = call.execute().body();
        } catch (IOException e) {
            Assert.fail("Exception " + e);
        }
        assertNotNull(parks);
        assertTrue(parks.size() > 0);
    }
}
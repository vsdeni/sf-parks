package com.vsdeni.sfparks.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class Location implements Serializable {
    @SerializedName("longitude")
    private double mLongitude;

    @SerializedName("latitude")
    private double mLatitude;

    @SerializedName("human_address")
    private Address mHumanAddress;

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public Address getHumanAddress() {
        return mHumanAddress;
    }

    public void setHumanAddress(Address humanAddress) {
        mHumanAddress = humanAddress;
    }

    public static class Address implements Serializable {
        @SerializedName("address")
        private String mAddress;

        @SerializedName("city")
        private String mCity;

        @SerializedName("state")
        private String mState;

        @SerializedName("zip")
        private String mZip;

        public String getAddress() {
            return mAddress;
        }

        public void setAddress(String address) {
            mAddress = address;
        }

        public String getCity() {
            return mCity;
        }

        public void setCity(String city) {
            mCity = city;
        }

        public String getState() {
            return mState;
        }

        public void setState(String state) {
            mState = state;
        }

        public String getZip() {
            return mZip;
        }

        public void setZip(String zip) {
            mZip = zip;
        }
    }
}

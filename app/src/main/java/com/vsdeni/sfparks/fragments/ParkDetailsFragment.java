package com.vsdeni.sfparks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vsdeni.sfparks.R;
import com.vsdeni.sfparks.activities.ToolbarActivity;
import com.vsdeni.sfparks.model.Location;
import com.vsdeni.sfparks.model.Park;

/**
 * Created by denisvasilenko on 22.02.16.
 */
public class ParkDetailsFragment extends Fragment {
    public static final String TAG = "DetailsFragment";

    TextView mTitleView;
    TextView mAddressView;
    TextView mAreaView;
    TextView mTypeView;
    TextView mAcreageView;

    Park mPark;

    private static final String PARK_KEY = "park";

    public static ParkDetailsFragment newInstance(Park park) {
        ParkDetailsFragment fragment = new ParkDetailsFragment();
        Bundle args = new Bundle(1);
        args.putSerializable(PARK_KEY, park);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_park_details, container, false);
        mAddressView = (TextView) view.findViewById(R.id.address);
        mAreaView = (TextView) view.findViewById(R.id.area);
        mTypeView = (TextView) view.findViewById(R.id.type);
        mAcreageView = (TextView) view.findViewById(R.id.acreage);
        mTitleView = (TextView) view.findViewById(R.id.title);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            mPark = (Park) savedInstanceState.getSerializable(PARK_KEY);
        } else {
            Bundle args = getArguments();
            if (args != null) {
                mPark = (Park) args.getSerializable(PARK_KEY);
            }
        }
        updateUI();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (getActivity() instanceof ToolbarActivity) {
            ((ToolbarActivity) getActivity()).updateToolbarWidgetsVisibility();
        }
        super.onActivityCreated(savedInstanceState);
    }

    private void updateUI() {
        if (mPark != null) {
            Location location = mPark.getLocation();
            if (location != null) {
                Location.Address address = location.getHumanAddress();
                StringBuilder builder = new StringBuilder();
                builder.append(address.getAddress())
                        .append(", ")
                        .append(address.getCity())
                        .append(", ")
                        .append(address.getState());
                if (!TextUtils.isEmpty(address.getZip())) {
                    builder.append(", ").append(address.getZip());
                }
                mAddressView.setText(builder.toString());
            }

            if (!TextUtils.isEmpty(mPark.getServiceArea())) {
                mAreaView.setText(getResources().getString(R.string.park_item_psa, mPark.getServiceArea()));
            }
            if (!TextUtils.isEmpty(mPark.getType())) {
                mTypeView.setText(getResources().getString(R.string.park_item_type, mPark.getType()));
            }
            if (mPark.getAcreage() > 0) {
                mAcreageView.setText(getResources().getString(R.string.park_item_acreage, String.valueOf(mPark.getAcreage())));
            }

            mTitleView.setText(mPark.getName());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PARK_KEY, mPark);
    }

    public void setPark(Park park) {
        mPark = park;
        updateUI();
    }
}

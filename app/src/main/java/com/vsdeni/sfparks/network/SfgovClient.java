package com.vsdeni.sfparks.network;

import com.vsdeni.sfparks.model.Park;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public interface SfgovClient {
    @GET("resource/z76i-7s65.json")
    Call<List<Park>> getParks();
}

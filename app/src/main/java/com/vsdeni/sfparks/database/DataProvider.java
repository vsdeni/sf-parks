package com.vsdeni.sfparks.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by denisvasilenko on 19.02.16.
 */
public class DataProvider extends ContentProvider {
    private DBHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = DBHelper.getInstance(getContext());
        return true;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public Uri insert(@NonNull Uri uri, @NonNull ContentValues values) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        long id = db.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_IGNORE);

        if (id != -1) {
            getContext().getContentResolver().notifyChange(uri, null);
            return Uri.withAppendedPath(uri, Long.toString(id));
        }

        return uri;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int delete(@NonNull Uri uri, @NonNull String selection, String[] selectionArgs) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int count = db.delete(tableName, selection, selectionArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int update(@NonNull Uri uri, @NonNull ContentValues values, String selection, String[] selectionArgs) {
        String tableName = getTableName(uri);

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int count = db.update(tableName, values, selection, selectionArgs);

        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @SuppressWarnings("ConstantConditions")
    public int bulkInsert(Uri uri, @NonNull ContentValues[] values) {
        int numInserted = 0;

        String tableName = getTableName(uri);

        SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
        sqlDB.beginTransaction();
        try {
            for (ContentValues cv : values) {
                long newID = sqlDB.insertOrThrow(tableName, null, cv);
                if (newID <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            sqlDB.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);

            numInserted = values.length;
        } catch (Exception e) {
            Log.e("", e.getMessage());
        } finally {
            sqlDB.endTransaction();
        }
        return numInserted;
    }

    private String getTableName(Uri uri) {
        String tableName;
        switch (ContentDescriptor.URI_MATCHER.match(uri)) {
            case ContentDescriptor.Parks.CODE_ALL:
            case ContentDescriptor.Parks.CODE_ID:
                tableName = ContentDescriptor.Parks.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Can't recognize uri!");
        }

        return tableName;
    }
}
